#include <fcntl.h>
#include <linux/module.h>
#include <stdio.h>
#include <string.h>
#include <sys/syscall.h>
#include <unistd.h>

#define finit_module(fd, param_values, flags) syscall(__NR_finit_module, fd, param_values, flags)
#define delete_module(name, flags) syscall(__NR_delete_module, name, flags)

#define TEST_NAME      printf("\nTEST: %s\n", __FUNCTION__);

#define NL "\n"

#define MODULE_NAME      "amyasnikov_fifo"
#define MODULE_NAME_FULL "./amyasnikov_fifo.ko"
#define DEVICE_NAME      "/dev/amyasnikov_fifo"

int fd;
int nbyte;
char buffer[1024];

#define message_1 "message_1"
#define message_2 "super_message_2"
#define message_3 "another message number 3"
#define message_pack message_1 NL message_2 NL message_3

void init() {
  int fd;

  fd = open(MODULE_NAME_FULL, O_RDONLY);
  if (finit_module(fd, "", 0) != 0) {
    perror("Can't init module \n");
  }

  close(fd);
}

void deinit() {
  if (delete_module(MODULE_NAME, O_NONBLOCK) != 0) {
    perror("Can't delete_module \n");
  }
}

void test_init_deinit() {
  init();
  deinit();
}

void test_open() {
  fd = open(DEVICE_NAME, O_RDWR);
  printf("open: fd: %d \n", fd);
  if (fd < 0) {
    perror("Can't open device \n");
  }
}

void test_write(const char* buf, size_t nbyte) {
  ssize_t ret;
  ret = write(fd, buf, nbyte);
  printf("write message: \t buffer: %.*s \t nbyte: %zd \t -> %ld \n", (int) nbyte, buf, nbyte, ret);
  if (ret != nbyte) {
    perror("Can't write message \n");
  }
}

void test_read(const char* buf, size_t nbyte) {
  ssize_t ret;
  ret = read(fd, buffer, sizeof(buffer));
  printf("read message: \t buffer: %.*s \t nbyte: %zd \t -> %ld \n", (int) ret, buffer, nbyte, ret);
  if (ret != nbyte) {
    perror("Can't read message \n");
  }
  if (memcmp(buffer, buf, ret)) {
    perror("Invalid message \n");
  }
}

void test_read_from_empty_fifo() {
  TEST_NAME;

  init();

  test_open();

  test_read("", 0);

  close(fd);

  deinit();
}

void test_read_write() {
  TEST_NAME;

  init();

  test_open();

  test_write(message_1, strlen(message_1));
  test_read(message_1, strlen(message_1));
  test_read("", 0);

  close(fd);

  deinit();
}

void test_read_write_many() {
  TEST_NAME;

  init();

  test_open();

  test_write(message_1, strlen(message_1));
  test_write(message_2, strlen(message_2));
  test_write(message_3, strlen(message_3));

  test_read(message_1, strlen(message_1));
  test_read(message_2, strlen(message_2));
  test_read(message_3, strlen(message_3));
  test_read("", 0);

  close(fd);

  deinit();
}

void test_read_write_pack() {
  TEST_NAME;

  init();

  test_open();

  test_write(message_pack, strlen(message_pack));

  test_read(message_1, strlen(message_1));
  test_read(message_2, strlen(message_2));
  test_read(message_3, strlen(message_3));
  test_read("", 0);

  close(fd);

  deinit();
}

int main() {
  test_init_deinit();
  test_read_from_empty_fifo();
  test_read_write();
  test_read_write_many();
  test_read_write_pack();
  return 0;
}
