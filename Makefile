obj-m += amyasnikov_fifo.o

$(eval VER=$(shell uname -r))

all: module client

module: amyasnikov_fifo.c
	make -C /lib/modules/${VER}/build M=$(PWD) modules

clean:
	make -C /lib/modules/${VER}/build M=$(PWD) clean
	rm -f client

client: client.c
	gcc client.c -o client
