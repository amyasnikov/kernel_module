#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/slab.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexander Myasnikov <myasnikov.alexander.s@gmail.com>");
MODULE_DESCRIPTION("Auriga linux kernel 2021 task");
MODULE_SUPPORTED_DEVICE("amyasnikov_fifo");

#define TRACEIN  TRACEMSG(KERN_INFO, "%s", ">>")
#define TRACEOUT TRACEMSG(KERN_INFO, "%s", "<<")
#define TRACEMSG(level, format, args...)   \
  printk(level "amyasnikov_fifo: %s: " format "\n", __FUNCTION__, args)


#define DEVICE_NAME "amyasnikov_fifo"

struct message_t {
  char* buf;
  size_t size;
  struct message_t* next;
};

struct messages_t {
  struct message_t* first;
  struct message_t* last;
};

static int device_open(struct inode*, struct file*);
static int device_release(struct inode*, struct file*);
static ssize_t device_read(struct file*, char*, size_t, loff_t*);
static ssize_t device_write(struct file*, const char*, size_t, loff_t*);

static int parse_message(const char*, size_t, size_t*, size_t*, size_t*);
static void messages_push_back(const char*, size_t);
static ssize_t messages_pop_front(char*, size_t);
static ssize_t message_copy(char*, size_t, struct message_t*);
static void message_free(struct message_t*);
static struct message_t* message_alloc(const char*, size_t);

static int   major;
static dev_t dev_num;
struct class *class;
static int   open_devices = 0;

static struct messages_t messages;

struct kmem_cache *pcache = NULL;

static struct file_operations fops = {
  .read = device_read,
  .write = device_write,
  .open = device_open,
  .release = device_release
};



static int __init fifo_init(void) {
  struct device *device;

  TRACEIN;

  major = register_chrdev(0, DEVICE_NAME, &fops);

  if (major < 0) {
    TRACEMSG(KERN_ERR, "Can't register device: %d", major);
    return major;
  }

  dev_num = MKDEV(major, 0);

  class = class_create(THIS_MODULE, DEVICE_NAME);
  if (class == NULL) {
    TRACEMSG(KERN_ERR, "Can't create class /sys/class/amyasinkov_fifo. device: %s", DEVICE_NAME);
    unregister_chrdev_region(dev_num, 1);
    return -1;
  }

  device = device_create(class, NULL, dev_num, NULL, DEVICE_NAME);
  if (device == NULL) {
    TRACEMSG(KERN_ERR, "Can't create device %s", DEVICE_NAME);
    class_destroy(class);
    unregister_chrdev_region(dev_num, 1);
    return -1;
  }

  pcache = kmem_cache_create("amyasnikov_cache", sizeof(struct message_t), 0, SLAB_HWCACHE_ALIGN, NULL);
  if (pcache == NULL) {
    TRACEMSG(KERN_ERR, "Can't create cache %s", "amyasnikov_cache");
    class_destroy(class);
    unregister_chrdev_region(dev_num, 1);
    return -1;
  }

  messages.first = NULL;
  messages.last = NULL;

  TRACEOUT;

  return 0;
}

static void __exit fifo_exit(void) {
  TRACEIN;

  while (messages.first) {
    struct message_t* message = messages.first;
    messages.first = message->next;
    message_free(message);
  }

  device_destroy(class, dev_num);
  class_destroy(class);
  unregister_chrdev(major, DEVICE_NAME);

  kmem_cache_destroy(pcache);

  TRACEOUT;
}



static int device_open(struct inode *inode, struct file *file) {
  TRACEIN;

  if (open_devices)
    return -EBUSY;

  open_devices++;

  if (!try_module_get(THIS_MODULE)) {
    TRACEMSG(KERN_ERR, "Can't get module: %p", THIS_MODULE);
  }

  TRACEOUT;

  return 0;
}

static int device_release(struct inode *inode, struct file *file) {
  TRACEIN;

  open_devices--;

  module_put(THIS_MODULE);

  TRACEOUT;

  return 0;
}

static ssize_t device_read(struct file *file, char *buf, size_t nbyte, loff_t *offset) {
  ssize_t ret;

  TRACEIN;

  ret = messages_pop_front(buf, nbyte);

  TRACEMSG(KERN_INFO, "buf: %p \t nbyte: %zd \t ret: %ld", buf, nbyte, ret);
  TRACEMSG(KERN_INFO, "message: %.*s", (int) ret, buf);

  TRACEOUT;

  return ret;
}

static ssize_t device_write(struct file *file, const char *buf, size_t nbyte, loff_t *offset) {
  size_t buf_offset = 0;
  size_t message_offset;
  size_t message_size;

  TRACEIN;

  TRACEMSG(KERN_INFO, "buf: %p \t nbyte: %zd ", buf, nbyte);

  while (!parse_message(buf, nbyte, &buf_offset, &message_offset, &message_size)) {
    TRACEMSG(KERN_INFO, "message: %.*s ", (int) message_size, buf + message_offset);
    messages_push_back(buf + message_offset, message_size);
  }

  TRACEOUT;

  return nbyte;
}



static int parse_message(const char* buf, size_t nbyte, size_t* offset, size_t* message_offset, size_t* message_size) {
  *message_offset = *offset;

  while (*offset < nbyte) {
    if (buf[*offset] == '\0' || buf[*offset] == '\n') {
      *message_size = *offset - *message_offset;
      (*offset)++;
      return 0;
    }

    (*offset)++;
  }

  *message_size = *offset - *message_offset;

  return *message_size == 0;
}

static void messages_push_back(const char* buf, size_t size) {
  struct message_t* message = message_alloc(buf, size);

  if (!message) {
    return;
  }

  if (!messages.last) { // 0 elements
    messages.first = message;
  } else { // 1+ elements
    messages.last->next = message;
  }

  messages.last = message;
}

static ssize_t messages_pop_front(char* buf, size_t size) {
  struct message_t* message = messages.first;
  ssize_t ret;

  if (!messages.last) { // 0 elements
    return 0;
  }

  ret = message_copy(buf, size, messages.first);

  if (ret < 0) {
    return ret;
  }

  if (messages.first == messages.last) { // 1 elements
    messages.first = NULL;
    messages.last = NULL;
  } else { // 2+ elements
    messages.first = messages.first->next;
  }

  message_free(message);

  return ret;
}

static ssize_t message_copy(char* buf, size_t size, struct message_t* message) {
  size_t ret;

  if (message->size < size) {
    memcpy(buf, message->buf, message->size);
    ret = message->size;
  } else {
    ret = -EMSGSIZE;
  }

  return ret;
}

static void message_free(struct message_t* message) {
  TRACEIN;

  if (message->buf) {
    TRACEMSG(KERN_INFO, "buffer: %p \t %zd", message->buf, message->size);
    kfree(message->buf);
  }

  TRACEMSG(KERN_INFO, "message: %p", message);
  kmem_cache_free(pcache, message);

  TRACEOUT;
}

static struct message_t* message_alloc(const char* buf, size_t size) {
  struct message_t* message;

  message = kmem_cache_alloc(pcache, GFP_KERNEL);
  TRACEMSG(KERN_INFO, "alloc: %p", message);

  if (!message) {
    TRACEMSG(KERN_ERR, "Can't alloc memory for message. size: %zd", size);
    return NULL;
  }

  message->buf = kmalloc(size, GFP_KERNEL);
  message->size = size;
  message->next = NULL;
  memcpy(message->buf, buf, size);

  TRACEMSG(KERN_INFO, "buffer: %p \t %zd", message->buf, size);

  if (!message->buf) {
    TRACEMSG(KERN_ERR, "Can't alloc memory for buffer. message: %p", message);
    message->buf = NULL;
    message->size = 0;
  }

  return message;
}


module_init(fifo_init);
module_exit(fifo_exit);
